package com.csam.icici.bank.imobile;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.huawei.hms.maps.CameraUpdate;
import com.huawei.hms.maps.CameraUpdateFactory;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapFragment;
import com.huawei.hms.maps.OnMapReadyCallback;
import com.huawei.hms.maps.model.BitmapDescriptorFactory;
import com.huawei.hms.maps.model.CameraPosition;
import com.huawei.hms.maps.model.Circle;
import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.maps.model.Marker;
import com.huawei.hms.maps.model.MarkerOptions;
import com.huawei.hms.maps.util.LogM;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String TAG = "MapViewDemoActivity";

    private static final String MAPVIEW_BUNDLE_KEY = "MapViewBundleKey";

    private static final int REQUEST_CODE = 100;

    private static final LatLng LAT_LNG = new LatLng(12.722924, 77.816707);
    private static final String[] RUNTIME_PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET};
    private HuaweiMap hmap;
    private Marker mMarker;
    private Circle mCircle;
    private MapFragment mMapFragment;

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LogM.d(TAG, "map onCreate:");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapview);

        if (!hasPermissions(this, RUNTIME_PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE);
        }
        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapView);
        mMapFragment.getMapAsync(this);
        /*// get mapView by layout view
        mMapView = findViewById(R.id.mapView);
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView.onCreate(mapViewBundle);

        // get map by async method
        mMapView.getMapAsync(this);*/
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMapReady(HuaweiMap map) {
        Log.d(TAG, "onMapReady: ");

        hmap = map;

        hmap.getUiSettings().setMyLocationButtonEnabled(false);
       /* hmap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(48.893478, 2.334595),10));*/

        /*hmap.setMaxZoomPreference(5);
        hmap.setMinZoomPreference(2);*/

        // mark can be add by HuaweiMap
       /* mMarker = hmap.addMarker(new MarkerOptions().position(LAT_LNG)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon))
                .clusterable(true));*/


        hmap.addMarker(new MarkerOptions().position(new LatLng(12.722924, 77.816707)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_1))
                .title("Marker1").clusterable(true));
        hmap.addMarker(new MarkerOptions().position(new LatLng(12.729241, 77.804522)).
                icon(BitmapDescriptorFactory.
                        fromResource(R.drawable.icon_2))
                .title("Marker2").clusterable(true));
        hmap.addMarker(new MarkerOptions().position(new LatLng(12.701094, 77.836601)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_3)).title("Marker3").clusterable(true));
        hmap.addMarker(new MarkerOptions().position(new LatLng(12.745468, 77.864631)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_4)).title("Marker4").clusterable(true));
        hmap.addMarker(new MarkerOptions().position(new LatLng(12.779373, 77.823209)).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_5)).title("Marker5").clusterable(true));
        hmap.addMarker(new MarkerOptions().position(new LatLng(12.732629, 77.826933)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_icon)).title("Marker6").clusterable(true));

        hmap.setMarkersClustering(true);

//        mMarker.showInfoWindow();
       hmap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(12.722924, 77.816707), 10));

        CameraPosition cameraPosition =
                CameraPosition.builder().target(new LatLng(12.722924, 77.816707)).zoom(15).bearing(2.0f).tilt(2.0f).build();
        // move camera by CameraPosition param ,latlag and zoom params can set here
        CameraPosition build = new CameraPosition.Builder().target(new LatLng(12.722924, 77.816707)).zoom(15).build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(build);
        hmap.animateCamera(cameraUpdate);
        // circle can be add by HuaweiMap
        //  mCircle = hmap.addCircle(new CircleOptions().center(new LatLng(12.717960, 77.8224040)));
    }



    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
