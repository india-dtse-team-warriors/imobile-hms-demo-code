package com.csam.icici.bank.imobile;

import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.site.api.SearchResultListener;
import com.huawei.hms.site.api.SearchService;
import com.huawei.hms.site.api.SearchServiceFactory;
import com.huawei.hms.site.api.model.AddressDetail;
import com.huawei.hms.site.api.model.Coordinate;
import com.huawei.hms.site.api.model.DetailSearchRequest;
import com.huawei.hms.site.api.model.DetailSearchResponse;
import com.huawei.hms.site.api.model.LocationType;
import com.huawei.hms.site.api.model.NearbySearchRequest;
import com.huawei.hms.site.api.model.NearbySearchResponse;
import com.huawei.hms.site.api.model.QuerySuggestionRequest;
import com.huawei.hms.site.api.model.QuerySuggestionResponse;
import com.huawei.hms.site.api.model.SearchStatus;
import com.huawei.hms.site.api.model.Site;
import com.huawei.hms.site.api.model.TextSearchRequest;
import com.huawei.hms.site.api.model.TextSearchResponse;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class SiteActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    TextView resultTextView;
    EditText queryInput;
    private SearchService searchService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.site_layout);

        searchService = SearchServiceFactory.create(this, "CV7iFTnZWsutEMMblV0gHDhIYHOusDrvmUIkcN%2BiF3jmFwluLBBnF%2FYjp%2FEwZGYAKA8Xeu3%2BpF4klWR%2BR4oyGU%2FVPUTl");

        queryInput = findViewById(R.id.edit_text_text_search_query);
        resultTextView = findViewById(R.id.response_text_search);

        findViewById(R.id.keyword_search_id).setOnClickListener(this);
        findViewById(R.id.nearby_place_search_id).setOnClickListener(this);
        findViewById(R.id.place_details_id).setOnClickListener(this);
        findViewById(R.id.place_search_suggestion_id).setOnClickListener(this);


    }

    public void keySearch() {

// Create a request body.
        TextSearchRequest request = new TextSearchRequest();
        request.setQuery(queryInput.getText().toString());
        Coordinate location = new Coordinate(12.927734, 77.593132);
        request.setLocation(location);
        request.setRadius(1000);
        request.setPoiType(LocationType.BANK);
        request.setCountryCode("IN");
        request.setLanguage("en");
        request.setPageIndex(1);
        request.setPageSize(5);
        searchService.textSearch(request, new SearchResultListener<TextSearchResponse>() {
            @Override
            public void onSearchResult(TextSearchResponse textSearchResponse) {

                StringBuilder response = new StringBuilder("\n");
                response.append("success\n");
                int count = 1;
                AddressDetail addressDetail;
                for (Site site : textSearchResponse.getSites()) {
                    addressDetail = site.getAddress();
                    response.append(String.format(
                            "[%s]  name: %s, formatAddress: %s, country: %s, countryCode: %s \r\n",
                            "" + (count++), site.getName(), site.getFormatAddress(),
                            (addressDetail == null ? "" : addressDetail.getCountry()),
                            (addressDetail == null ? "" : addressDetail.getCountryCode())));
                }
                Log.d(TAG, "search result is : " + response);
                resultTextView.setText(response.toString());
            }

            @Override
            public void onSearchError(SearchStatus searchStatus) {
                Log.e(TAG, "onSearchError is: " + searchStatus.getErrorCode());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.keyword_search_id:
               keySearch();
             /*   //distanceBetweenLocation();
                distance(Double.parseDouble("12.927734"),Double.parseDouble("77.593132"),Double.parseDouble("12.657024"),Double.parseDouble("77.895063"));
              //  distance(Float.parseFloat("12.927734"),Float.parseFloat("77.593132"),Float.parseFloat("12.657024"),Float.parseFloat("77.895063"));
                LatLng mylocation  = new LatLng(Double.parseDouble("12.927734"),Double.parseDouble("77.593132"));
                LatLng dest_location  = new LatLng(Double.parseDouble("12.657024"),Double.parseDouble("77.895063"));
                distanceBetween(mylocation,dest_location);*/
                break;
            case R.id.nearby_place_search_id:
                nearByPlaceSearch();
                break;
            case R.id.place_details_id:
                placeDetails();
                break;
            case R.id.place_search_suggestion_id:
                placeSearchSuggestions();
                break;
        }

    }

    private void distanceBetweenLocation() {
            Location mylocation = new Location("12.927734, 77.593132");
        Location dest_location = new Location("12.657024, 77.895063");
        String lat = "12.657024";
        String lon = "77.89506";
        dest_location.setLatitude(Double.parseDouble(lat));
        dest_location.setLongitude(Double.parseDouble(lon));
        mylocation.setLatitude(12.927734);
        mylocation.setLongitude(77.593132);
        float distance = mylocation.distanceTo(dest_location);//in meters
        Toast.makeText(this, "Distance"+Double.toString(distance),
                Toast.LENGTH_LONG).show();


    }

    private float distanceBetween(LatLng latLng1, LatLng latLng2) {
        Location loc1 = new Location(LocationManager.GPS_PROVIDER);
        Location loc2 = new Location(LocationManager.GPS_PROVIDER);
        loc1.setLatitude(latLng1.latitude);
        loc1.setLongitude(latLng1.longitude);
        loc2.setLatitude(latLng2.latitude);
        loc2.setLongitude(latLng2.longitude);
        return loc1.distanceTo(loc2);
    }

    private String distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.DOWN);
        return decimalFormat.format(dist* 1.60934) + " KMS";
    }

    public static float distance(float lat1, float lng1, float lat2, float lng2) {

        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
    private void placeSearchSuggestions() {

        QuerySuggestionRequest request = new QuerySuggestionRequest();
        request.setQuery(queryInput.getText().toString());
        Coordinate location = new Coordinate(12.927734, 77.593132);
        request.setLocation(location);
        request.setRadius(1000);
        request.setCountryCode("IN");
        request.setLanguage("en");
// Create a search result listener.
        SearchResultListener<QuerySuggestionResponse> resultListener = new SearchResultListener<QuerySuggestionResponse>() {
            // Return search results upon a successful search.
            @Override
            public void onSearchResult(QuerySuggestionResponse results) {
                List<Site> sites = results.getSites();
                if (results == null || sites == null || sites.size() <= 0) {
                    return;
                }
                for (Site site : sites) {
                    Log.i("TAG", String.format("siteId: '%s', name: %s\r\n", site.getSiteId(), site.getName()));
                }
            }

            // Return the result code and description upon a search exception.
            @Override
            public void onSearchError(SearchStatus status) {
                Log.i("TAG", "Error : " + status.getErrorCode() + " " + status.getErrorMessage());
            }
        };
// Call the search suggestion API.
        searchService.querySuggestion(request, resultListener);
    }

    private void placeDetails() {

// Create a request body.
        DetailSearchRequest request = new DetailSearchRequest();
        request.setSiteId("XXX");
        request.setLanguage("fr");
// Create a search result listener.
        SearchResultListener<DetailSearchResponse> resultListener = new SearchResultListener<DetailSearchResponse>() {
            // Return search results upon a successful search.
            @Override
            public void onSearchResult(DetailSearchResponse results) {
                Site site = results.getSite();
                if (results == null || site == null) {
                    return;
                }
                Log.i("TAG", String.format("siteId: '%s', name: %s\r\n", site.getSiteId(), site.getName()));
            }

            // Return the result code and description upon a search exception.
            @Override
            public void onSearchError(SearchStatus status) {
                Log.i("TAG", "Error : " + status.getErrorCode() + " " + status.getErrorMessage());
            }
        };
// Call the nearby place search API.
        searchService.detailSearch(request, resultListener);
    }

    private void nearByPlaceSearch() {

// Create a request body.
        NearbySearchRequest request = new NearbySearchRequest();
        Coordinate location = new Coordinate(48.893478, 2.334595);
        request.setLocation(location);
        request.setQuery("Paris");
        request.setRadius(1000);
        request.setPoiType(LocationType.ADDRESS);
        request.setLanguage("fr");
        request.setPageIndex(1);
        request.setPageSize(5);
// Create a search result listener.
        SearchResultListener<NearbySearchResponse> resultListener = new SearchResultListener<NearbySearchResponse>() {
            // Return search results upon a successful search.
            @Override
            public void onSearchResult(NearbySearchResponse results) {
                List<Site> sites = results.getSites();
                if (results == null || results.getTotalCount() <= 0 || sites == null || sites.size() <= 0) {
                    return;
                }
                for (Site site : sites) {
                    Log.i("TAG", String.format("siteId: '%s', name: %s\r\n", site.getSiteId(), site.getName()));
                }
            }

            // Return the result code and description upon a search exception.
            @Override
            public void onSearchError(SearchStatus status) {
                Log.i("TAG", "Error : " + status.getErrorCode() + " " + status.getErrorMessage());
            }
        };
// Call the nearby place search API.
        searchService.nearbySearch(request, resultListener);
    }
}
