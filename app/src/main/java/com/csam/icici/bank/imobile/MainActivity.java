package com.csam.icici.bank.imobile;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.huawei.agconnect.config.AGConnectServicesConfig;
import com.huawei.agconnect.crash.AGConnectCrash;
import com.huawei.hianalytics.hms.HiAnalyticsTools;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.ml.scan.HmsScan;
import com.huawei.hms.ml.scan.HmsScanAnalyzerOptions;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    public static final int DEFAULT_VIEW = 0x22;
    private static final int REQUEST_CODE_SCAN = 0X01;
    private static final String TAG = "MainActivity";
    private HiAnalyticsInstance instance;
    private String pushtoken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.crash_id).setOnClickListener(this);
        findViewById(R.id.analytics_id).setOnClickListener(this);
        findViewById(R.id.push_id).setOnClickListener(this);
        findViewById(R.id.location_id).setOnClickListener(this);
        findViewById(R.id.map_id).setOnClickListener(this);
        findViewById(R.id.site_id).setOnClickListener(this);
        findViewById(R.id.scan_kit).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.crash_id:

                AGConnectCrash.getInstance().testIt();
                break;
            case R.id.analytics_id:
                HiAnalyticsTools.enableLog();
                // Generate the Analytics Instance
                instance = HiAnalytics.getInstance(this);

                // Enable collection capability
                instance.setAnalyticsEnabled(true);
                reportAnswerEvt();
                postScore();
                break;


            case R.id.push_id:
                getToken();
                break;
            case R.id.location_id:
                Intent intent = new Intent(MainActivity.this, LocationUpdateActivity.class);
                startActivity(intent);
                break;
            case R.id.scan_kit:
                // DEFAULT_VIEW is customized for receiving the permission verification result.
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE},
                        DEFAULT_VIEW);

                break;

            case R.id.map_id:
                Intent callIntent = new Intent(MainActivity.this, MapActivity.class);
                startActivity(callIntent);
                break;
            case R.id.site_id:
                Intent siteIntent = new Intent(MainActivity.this, SiteActivity.class);
                startActivity(siteIntent);


            default:
                break;
        }
    }


    private void reportAnswerEvt() {

        // Initialize parameters.
        Bundle bundle = new Bundle();
        bundle.putString("mobile_number", "9880107877");
        bundle.putString("loginClickEvent", "Clicked");
        // Report a predefined event.
        instance.onEvent("LoginBtEvent", bundle);
    }

    private void postScore() {
        //Customize tracing points and add tracing points in the proper position of the code.
        /*Bundle bundle = new Bundle();
        bundle.putString("exam_difficulty","high");
        bundle.putString("exam_level","1-1");
        bundle.putString("exam_time","20190520-08");
        instance.onEvent("begin_examination", bundle);

        Bundle bundle_pre = new Bundle();
        bundle_pre.putString(PRODUCTID, "item_ID");
        bundle_pre.putString(PRODUCTNAME, "name");
        bundle_pre.putString(CATEGORY, "category");
        bundle_pre.putLong(QUANTITY, 100L);
        bundle_pre.putDouble(PRICE, 10.01);
        bundle_pre.putDouble(REVENUE, 10);
        bundle_pre.putString(CURRNAME, "currency");
        bundle_pre.putString(PLACEID, "location_ID");
        instance.onEvent(ADDPRODUCT2WISHLIST, bundle_pre);*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || grantResults == null || grantResults.length < 2 || grantResults[0] !=
                PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (requestCode == 1) {
            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "onRequestPermissionsResult: apply LOCATION PERMISSION successful");
            } else {
                Log.i(TAG, "onRequestPermissionsResult: apply LOCATION PERMISSSION  failed");
            }
        }

        if (requestCode == 2) {
            if (grantResults.length > 2 && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "onRequestPermissionsResult: apply ACCESS_BACKGROUND_LOCATION successful");
            } else {
                Log.i(TAG, "onRequestPermissionsResult: apply ACCESS_BACKGROUND_LOCATION  failed");
            }
        }

        if (requestCode == DEFAULT_VIEW) {

            ScanUtil.startScan(this, REQUEST_CODE_SCAN,
                    new HmsScanAnalyzerOptions.Creator().setHmsScanTypes(HmsScan.ALL_SCAN_TYPE).create());
        }

    }


    private void getToken() {
        Log.i(TAG, "get token: begin");

        // get token
        new Thread() {
            @Override
            public void run() {
                try {
                    // read from agconnect-services.json
                    String appId = AGConnectServicesConfig.fromContext(MainActivity.this).getString("client/app_id");
                    pushtoken = HmsInstanceId.getInstance(MainActivity.this).getToken(appId, "HCM");
                    if(!TextUtils.isEmpty(pushtoken)) {
                        Log.i(TAG, "get token:" + pushtoken);
                        sendRegTokenToServer(pushtoken);
                    }
                } catch (Exception e) {
                    Log.i(TAG,"getToken failed, " + e);

                }
            }
        }.start();
    }

    private void sendRegTokenToServer(String token) {

        Log.i("IciciDemo", "sending token to server. token:" + token);
        Toast.makeText(this, "Push Token: " + token, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //receive result after your activity finished scanning
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) {
            return;
        }
        // Obtain the return value of HmsScan from the value returned by the onActivityResult method by using ScanUtil.RESULT as the key value.
        if (requestCode == REQUEST_CODE_SCAN) {
            Object obj = data.getParcelableExtra(ScanUtil.RESULT);
            if (obj instanceof HmsScan) {
                if (!TextUtils.isEmpty(((HmsScan) obj).getOriginalValue())) {
                    Toast.makeText(this, ((HmsScan) obj).getOriginalValue(), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
